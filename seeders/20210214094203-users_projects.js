'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [
      {
        id: 'a8d120e1-5f6f-4f81-81f8-79c6c56f1160',
        email: 'test@test.com',
        password: '$2a$10$TCVo6a9Vojxava0UVYUHKOEE.G/un5f0C23GWzBYFA2OY6uQSMsCq',
        createdAt: '2021-02-14 09:46:34.810 +00:00',
        updatedAt: '2021-02-14 09:46:34.810 +00:00',
      },
      {
        id: '777b9d97-2db2-47ed-a682-4d52f5952ff3',
        email: 'roman@code-lab.si',
        password: '$2a$10$uLFNFyrJ/Dascx4T3oM8ruA27fEJk1YmScYGxKreASezwdtZ40iAK',
        createdAt: '2021-02-14 09:46:52.002 +00:00',
        updatedAt: '2021-02-14 09:46:52.002 +00:00',
      },
    ], {})
    await queryInterface.bulkInsert('projects', [
      {
        id: 'f8180b12-d9f4-4bf2-b7ab-c9408ab71ae5',
        name: 'Hitri račun - APP',
        userId: '777b9d97-2db2-47ed-a682-4d52f5952ff3',
        createdAt: '2021-02-14 09:54:27.134 +00:00',
        updatedAt: '2021-02-14 09:54:27.134 +00:00',
      },
      {
        id: '1fab5285-795c-4a77-ace7-eed93f97f58b',
        name: 'Hitri račun - CMS',
        userId: '777b9d97-2db2-47ed-a682-4d52f5952ff3',
        createdAt: '2021-02-14 09:54:27.134 +00:00',
        updatedAt: '2021-02-14 09:54:27.134 +00:00',
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {})
    await queryInterface.bulkDelete('projects', null, {})
  },
}
