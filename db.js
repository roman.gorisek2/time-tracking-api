const { sequelize } = require('./models')

exports.connectDB = async () => {
  try {
    await sequelize.authenticate()
    console.info('connection to the database has been established successfully')
  } catch (err) {
    console.info('unable to connect to the database:', err)
  }
}
