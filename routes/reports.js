const express = require('express')
const {
  monthly,
} = require('../controllers/reports')

const router = express.Router({ mergeParams: true })
const { protect } = require('../middleware/auth')

router.use(protect)

router
  .route('/')
  .get(monthly)

module.exports = router
