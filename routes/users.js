const express = require('express')
const {
  getUsers,
  getUser,
  deleteUser,
} = require('../controllers/users')

const router = express.Router({ mergeParams: true })
const { protect } = require('../middleware/auth')

router.use(protect)

router
  .route('/')
  .get(getUsers)

router
  .route('/:id')
  .get(getUser)
  .delete(deleteUser)

module.exports = router
