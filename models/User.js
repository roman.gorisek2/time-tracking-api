'use strict'
const bcryptService = require('../services/bcrypt.service')
const {
  Model,
  Sequelize,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate ({ Task, Project }) {
      this.hasMany(Task, { foreignKey: 'userId' })
      this.hasMany(Project, { foreignKey: 'userId' })
    }

    toJSON () {
      return { ...this.get(), password: undefined }
    }
  };
  User.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set (val) {
        const hashed = bcryptService().password(val)
        this.setDataValue('password', hashed)
      },
    },
  }, {
    sequelize,
    tableName: 'users',
    modelName: 'User',
  })
  return User
}
