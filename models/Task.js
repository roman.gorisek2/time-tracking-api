'use strict'
const {
  Model,
  Sequelize,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate ({ User, Project }) {
      this.belongsTo(User, { foreignKey: 'userId', as: 'user' })
      this.belongsTo(Project, { foreignKey: 'projectId', as: 'project' })
    }

    toJSON () {
      return { ...this.get() }
    }
  };
  Task.init({
    id: {
      allowNull: false,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    timeAllocated: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    timeSpent: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    taskDate: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    sequelize,
    tableName: 'tasks',
    modelName: 'Task',
  })
  return Task
}
