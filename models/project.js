'use strict'
const {
  Model,
  Sequelize,
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Project extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate ({ Task, User }) {
      this.belongsTo(User, { foreignKey: 'userId', as: 'user' })
      this.hasMany(Task, { foreignKey: 'projectId' })
    }
  };
  Project.init({
    id: {
      allowNull: false,
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  }, {
    sequelize,
    tableName: 'projects',
    modelName: 'Project',
  })
  return Project
}
