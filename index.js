const express = require('express')
const morgan = require('morgan')
const helmet = require('helmet')
const dotenv = require('dotenv')
const xss = require('xss-clean')
const rateLimit = require('express-rate-limit')
const hpp = require('hpp')
const cors = require('cors')
const errorHandler = require('./middleware/error')
const { connectDB } = require('./db')

dotenv.config({ path: './config/config.env' })

connectDB()

const app = express()
app.use(express.json())

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

app.use(cors())
app.use(helmet())
app.use(xss())
const limiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 mins
  max: 100,
})
app.use(limiter)
app.use(hpp())

const auth = require('./routes/auth')
const users = require('./routes/users')
const tasks = require('./routes/tasks')
const projects = require('./routes/projects')
const reports = require('./routes/reports')
app.use('/api/auth', auth)
app.use('/api/users', users)
app.use('/api/tasks', tasks)
app.use('/api/projects', projects)
app.use('/api/reports', reports)

app.use(errorHandler)

const PORT = process.env.PORT || 5000

app.listen(
  PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`,
  ),
)

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red)
  // Close server & exit process
  // server.close(() => process.exit(1));
})
