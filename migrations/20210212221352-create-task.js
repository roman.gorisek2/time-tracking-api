'use strict'
const {
  Sequelize,
} = require('sequelize')
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('tasks', {
      id: {
        allowNull: false,
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      timeAllocated: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      timeSpent: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      taskDate: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      projectId: {
        type: DataTypes.UUID,
        references: {
          model: 'projects',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    })
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('tasks')
  },
}
