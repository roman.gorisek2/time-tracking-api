const ErrorResponse = require('../utils/errorResponse')
const asyncHandler = require('../middleware/async')
const { Task, Project } = require('../models')
const { Op } = require('sequelize')

exports.getTasks = asyncHandler(async (req, res, next) => {
  const filters = {
    userId: req.user.id,
  }
  if (req.query.dateFrom && req.query.dateTo) {
    filters.taskDate = {
      [Op.between]: [req.query.dateFrom, req.query.dateTo],
    }
  }
  const tasks = await Task.findAll({ where: filters, include: { model: Project, as: 'project', attributes: ['id', 'name'] } })

  res.status(200).json({
    success: true,
    data: tasks,
  })
})

exports.getTask = asyncHandler(async (req, res, next) => {
  const task = await Task.findByPk(req.params.id)

  res.status(200).json({
    success: true,
    data: task,
  })
})

exports.createTask = asyncHandler(async (req, res, next) => {
  const {
    title, timeAllocated, timeSpent, taskDate, projectId,
  } = req.body
  const userId = req.user.id

  const task = await Task.create({
    title,
    timeAllocated,
    timeSpent,
    taskDate,
    userId,
    projectId,
  })

  if (!task) {
    return next(new ErrorResponse('Task could not be created', 400))
  }

  res.status(200).json({
    success: true,
    data: task,
  })
})

exports.updateTask = asyncHandler(async (req, res, next) => {
  const { id } = req.params
  const {
    title, timeAllocated, timeSpent, taskDate, projectId,
  } = req.body

  const task = await Task.findByPk(id)

  if (!task) {
    return next(new ErrorResponse('Task not found', 400))
  }

  const updatedTask = await task.update({
    title,
    timeAllocated,
    timeSpent,
    taskDate,
    projectId,
  })

  if (!updatedTask) {
    return next(new ErrorResponse('Task could not be updated', 400))
  }

  res.status(200).json({
    success: true,
    data: updatedTask,
  })
})

exports.deleteTask = asyncHandler(async (req, res, next) => {
  const task = await Task.findByPk(req.params.id)
  if (!task) {
    return res.status(200).json({
      success: true,
      data: null,
    })
  }

  await task.destroy()

  res.status(200).json({
    success: true,
    data: null,
  })
})
