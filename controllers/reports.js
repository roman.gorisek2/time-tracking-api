const asyncHandler = require('../middleware/async')
const { Task, Project } = require('../models')
const { Op } = require('sequelize')
const moment = require('moment')

exports.monthly = asyncHandler(async (req, res, next) => {
  const filters = {
    userId: req.user.id,
  }
  const firstDayOfMonth = moment().startOf('month').startOf('day').format()
  const lastDayOfMonth = moment().endOf('month').endOf('day').format()
  const dateFrom = req.query.dateFrom || firstDayOfMonth
  const dateTo = req.query.dateTo || lastDayOfMonth

  filters.taskDate = {
    [Op.between]: [dateFrom, dateTo],
  }
  console.log(filters)
  const tasks = await Task.findAll({ where: filters, include: { model: Project, as: 'project', attributes: ['id', 'name'] } })

  const report = {
    year: moment(dateFrom).format('YYYY'),
    month: moment(dateFrom).format('MM'),
    projects: {},
  }

  tasks.forEach(task => {
    if (!task.project) {
      task.project = {}
      task.project.id = 'other'
      task.project.name = 'Other'
    }
    if (!report.projects[task.project.id]) {
      report.projects[task.project.id] = {
        id: task.project.id,
        name: task.project.name,
        timeAllocated: 0,
        timeSpent: 0,
        tasks: [],
      }
    }
    report.projects[task.project.id].timeAllocated += task.timeAllocated
    report.projects[task.project.id].timeSpent += task.timeSpent
    report.projects[task.project.id].tasks.push({
      id: task.id,
      taskDate: task.taskDate,
      timeAllocated: task.timeAllocated,
      timeSpent: task.timeSpent,
      title: task.title,
    })
  })
  report.projects = Object.values(report.projects)

  res.status(200).json({
    success: true,
    data: [report],
  })
})
