const asyncHandler = require('../middleware/async')
const { User } = require('../models')

exports.getUsers = asyncHandler(async (req, res, next) => {
  const users = await User.findAll()
  res.status(200).json({ users })
})

exports.getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findByPk(req.params.id)

  res.status(200).json({
    success: true,
    data: user,
  })
})

exports.deleteUser = asyncHandler(async (req, res, next) => {
  const user = await User.findByPk(req.params.id)
  if (!user) {
    return res.status(200).json({
      success: true,
      data: {},
    })
  }

  await user.destroy()

  res.status(200).json({
    success: true,
    data: {},
  })
})
