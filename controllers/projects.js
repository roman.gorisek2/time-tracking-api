const ErrorResponse = require('../utils/errorResponse')
const asyncHandler = require('../middleware/async')
const { Project } = require('../models')

exports.getProjects = asyncHandler(async (req, res, next) => {
  const filters = {
    userId: req.user.id,
  }
  const projects = await Project.findAll({ where: filters })

  res.status(200).json({
    success: true,
    data: projects,
  })
})

exports.getProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findByPk(req.params.id)

  res.status(200).json({
    success: true,
    data: project,
  })
})

exports.createProject = asyncHandler(async (req, res, next) => {
  const {
    name,
  } = req.body
  const userId = req.user.id

  const project = await Project.create({
    name,
    userId,
  })

  if (!project) {
    return next(new ErrorResponse('Project could not be created', 400))
  }

  res.status(200).json({
    success: true,
    data: project,
  })
})

exports.updateProject = asyncHandler(async (req, res, next) => {
  const { id } = req.params
  const {
    name,
  } = req.body

  const project = await Project.findByPk(id)

  if (!project) {
    return next(new ErrorResponse('Project not found', 400))
  }

  const updatedProject = await project.update({
    name,
  })

  if (!updatedProject) {
    return next(new ErrorResponse('Project could not be updated', 400))
  }

  res.status(200).json({
    success: true,
    data: updatedProject,
  })
})

exports.deleteProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findByPk(req.params.id)
  if (!project) {
    return res.status(200).json({
      success: true,
      data: null,
    })
  }

  await project.destroy()

  res.status(200).json({
    success: true,
    data: null,
  })
})
